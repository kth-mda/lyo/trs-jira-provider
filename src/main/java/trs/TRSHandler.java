/*
 * Copyright (c) 2017  Xufei Ning.
 * 
 * All rights reserved. This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 and Eclipse Distribution License v. 1.0 which
 * accompanies this distribution.
 * 
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html and the
 * Eclipse Distribution License is available at http://www.eclipse.org/org/documents/edl-v10.php.
 */

package trs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.hp.hpl.jena.rdf.model.Model;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.lyo.core.trs.Base;
import org.eclipse.lyo.core.trs.ChangeEvent;
import org.eclipse.lyo.core.trs.ChangeLog;
import org.eclipse.lyo.core.trs.Creation;
import org.eclipse.lyo.core.trs.Deletion;
import org.eclipse.lyo.core.trs.Modification;
import org.eclipse.lyo.core.trs.Page;
import org.eclipse.lyo.core.trs.TRSConstants;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.provider.jena.JenaModelHelper;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class TRSHandler {

    public final static String server_uri = "http://localhost:8080/Jira-TRS/services/trs"; // TRS
    // USER SET
    private final static String username = "andrew"; // Jira
    private final static String password = "admin"; // Jira
    private final static String JIRA_BASE = "http://localhost:2990"; // Jira base
	// server
    private final static String issues = "http://localhost:8081/adaptor-jira/services/serviceProviders/1" +
			"/service1/changeRequests/"; // Jira adaptor
    // modify if necessary
    final static String jira_api = JIRA_BASE + "/rest/api/2/search"; // Jira API
    final static String base_uri = server_uri + "/base"; // TRS base
    final static String changeLog_uri = server_uri + "/changeLog/"; // TRS changeLog
    public static ChangeLog changeLog = new ChangeLog();
    static String MQTT_BROKER_ENDPOINT = "tcp://localhost:1883"; // MQTT broker
    // END OF USER SET
    static String MQTT_topic = "TRS"; // MQTT broker
    static int memberPerPage = 50; // member per page
    static int changeLogPerPage = 50; // changeLog per page
    static int regenerateTime = 3600000; // regenerate base time (ms)
    // base
    static List<URI> members = new ArrayList<URI>(); // Jira adaptor
    static List<URI> original_members = new ArrayList<URI>(); // Jira API
    static Map<String, Base> baseResouces = new HashMap<String, Base>();
    static int total;
    static String lastUpdate = null;
    // change log
    static int changeOrder;
    static Map<String, ChangeLog> changeLogs = new HashMap<String, ChangeLog>();

    static boolean run = false; // server statue
    static boolean newChangeEvent = false; // regenerate base

    // JQL connector
    public static JSONObject connector(URI uri) throws ClientProtocolException, IOException {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(uri);
        String encoding = Base64.getEncoder().encodeToString(
                (username + ":" + password).getBytes());
        request.setHeader("Authorization", "Basic " + encoding);
        request.setHeader("Accept", "application/json");
        HttpResponse response = client.execute(request);

        InputStream inputStream = response.getEntity().getContent();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonData = new HashMap<String, Object>();
        jsonData = mapper.readValue(inputStream, Map.class);
        JSONObject obj = new JSONObject(jsonData);

        return obj;
    }

    // generate base
    public static void generateBase()
            throws JSONException, ClientProtocolException, URISyntaxException, IOException,
            ParseException {
        List<URI> members = getMembers();
        int totalPage = total / memberPerPage; // total page
        int rest = total % memberPerPage; // members in the last page
        if (rest != 0) {
            totalPage++;
        }
        // only or less than one page
        if (total == memberPerPage || total < memberPerPage) {
            Base base = new Base();
            base.setAbout(URI.create(base_uri));
            base.setMembers(members);
            base.setCutoffEvent(
                    URI.create("urn:urn-3:cm1.example.com:" + lastUpdate + ":" + changeOrder));
            base.setNextPage(null);
            baseResouces.put(String.valueOf(1), base);
        }
        // multiple page
        else {
            for (int i = 1; i <= totalPage; i++) {
                Base base = new Base();
                base.setAbout(URI.create(base_uri));
                if (i != totalPage) {
                    List<URI> subChanges = members.subList(((i - 1) * memberPerPage),
                            ((i * memberPerPage)));
                    base.setMembers(subChanges);
                    Page nextPage = new Page();
                    nextPage.setAbout(URI.create(base_uri + (i + 1)));
                    base.setNextPage(nextPage);
                }
                // first page has cutoff event
                if (i == 1) {
                    base.setCutoffEvent(URI.create(
                            "urn:urn-3:cm1.example.com:" + lastUpdate + ":" + changeOrder));
                }
                // last page
                if (i == totalPage) {
                    List<URI> subChanges = members.subList(((i - 1) * memberPerPage), total);
                    base.setMembers(subChanges);
                    base.setNextPage(null);
                }
                baseResouces.put(String.valueOf(i), base);
            }
        }
    }

    // base page content
    public static Page getPageResource(String pagenum)
            throws JSONException, ClientProtocolException, URISyntaxException, IOException,
            ParseException {
        Page page = new Page();
        page.setAbout(URI.create(base_uri + "/" + pagenum));
        Base base = baseResouces.get(pagenum);
        page.setPageOf(base);
        Page nextPage = base.getNextPage();
        if (nextPage != null) {
            page.setNextPage(URI.create(base_uri + "/" + (Integer.parseInt(pagenum) + 1)));
        }
        if (nextPage == null) {
            page.setNextPage(new URI(TRSConstants.RDF_NIL));
        }

        return page;
    }

    // regenerate base
    public static void baseTimer() {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(regenerateTime); // time
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (newChangeEvent) {
                    System.out.println("Regenerate Base");
                    try {
                        original_members = new ArrayList<URI>(); // reset member
                        members = new ArrayList<URI>(); // reset member
                        generateBase();
                        newChangeEvent = false; // reset
                    } catch (JSONException | IOException | URISyntaxException | ParseException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    System.out.println("No new change event");
                }
            }
        }).start();
    }

    // add change event
    public static void addChangeEvent(String type, JSONObject obj)
            throws ClientProtocolException, IOException, URISyntaxException, MqttException,
            JSONException, ParseException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, DatatypeConfigurationException,
            OslcCoreApplicationException {
        System.out.println("Change order: " + ++changeOrder + ". Type: " + type);
        URI uri = new URI(obj.getString("self"));
        JSONObject fields = obj.getJSONObject("fields");
        String updateTime = null;
        if (type.equals("create")) {
            updateTime = fields.getString("created");
        }
        if (type.equals("modify")) {
            updateTime = fields.getString("updated");
        }
        if (type.equals("delete")) {
            DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Calendar cal = Calendar.getInstance();
            updateTime = dateformat.format(cal.getTime());
        }
        System.out.println("UPDATE: " + updateTime);
        System.out.println("URI: " + uri);

        // create change event, change URI
        ChangeEvent ce = null;
        String uri_string = uri.toString();
        String[] uri_string_split = uri_string.split("/");
        String id = uri_string_split[uri_string_split.length - 1];
        URI new_uri = new URI(issues + id);
        // three type: create, modify, delete
        if (type.equals("create")) {
            ce = new Creation(
                    URI.create("urn:urn-3:cm1.example.com:" + updateTime + ":" + changeOrder),
                    new_uri, changeOrder);
        }
        if (type.equals("modify")) {
            ce = new Modification(
                    URI.create("urn:urn-3:cm1.example.com:" + updateTime + ":" + changeOrder),
                    new_uri, changeOrder);
        }
        if (type.equals("delete")) {
            ce = new Deletion(
                    URI.create("urn:urn-3:cm1.example.com:" + updateTime + ":" + changeOrder),
                    new_uri, changeOrder);
        }

        lastUpdate = updateTime; // set update time
        changeLog.getChange().add(ce);
        newChangeEvent = true;

        // send ChangeEvent to MQTT, use Jena Model Helper
        Model changeEventJenaModel = JenaModelHelper.createJenaModel(new Object[]{ce});
        MqttClient client = new MqttClient(MQTT_BROKER_ENDPOINT, "jira-trs-publisher");
        client.connect();
        MqttMessage message = new MqttMessage();
        message.setPayload(changeEventJenaModel.toString().getBytes());
        try {
            //TODO remove!
            Thread.sleep(200);
        } catch (InterruptedException e) {

        }
        client.publish(MQTT_topic, message);
        client.disconnect();
    }

    // generate changeLog
    public static void generateChangeLog() throws URISyntaxException {
        List<ChangeEvent> changes = Lists.reverse(changeLog.getChange()); // reverse
        int total = changes.size();
        int totalPage = total / changeLogPerPage; // total page
        int rest = total % changeLogPerPage; // change events in the last page
        if (rest != 0) {
            totalPage++;
        }
        // only or less than one page
        if (total == changeLogPerPage || total < changeLogPerPage) {
            ChangeLog changeLog = new ChangeLog();
            changeLog.setChange(changes);
            changeLog.setPrevious(new URI(TRSConstants.RDF_NIL));
            changeLogs.put(String.valueOf(1), changeLog);
        }
        // multiple page
        else {
            for (int i = 1; i <= totalPage; i++) {
                ChangeLog changeLog = new ChangeLog();
                if (i != totalPage) {
                    List<ChangeEvent> subChanges = changes.subList(((i - 1) * changeLogPerPage),
                            ((i * changeLogPerPage)));
                    changeLog.setChange(subChanges);
                    changeLog.setPrevious(URI.create(changeLog_uri + (i + 1)));
                }
                if (i == totalPage) {
                    List<ChangeEvent> subChanges = changes.subList(((i - 1) * changeLogPerPage),
                            total);
                    changeLog.setChange(subChanges);
                    changeLog.setPrevious(new URI(TRSConstants.RDF_NIL));
                }
                changeLogs.put(String.valueOf(i), changeLog);
            }
        }
    }

    // changeLog content
    public static ChangeLog getChangeLog(String pagenum) throws URISyntaxException {
        ChangeLog changeLog = changeLogs.get(pagenum);
        changeLog.setAbout(URI.create(changeLog_uri + pagenum));
        return changeLog;
    }

    // get base member
    public static List<URI> getMembers()
            throws JSONException, URISyntaxException, ClientProtocolException, IOException,
            ParseException {
        JSONObject checkTotal = TRSHandler.connector(new URI(jira_api + "?maxResults=0"));
        total = checkTotal.getInt("total");

        int startAt = 0;
        int maxResults = 50;

        while (true) {
            JSONObject obj = TRSHandler.connector(
                    new URI(jira_api + "?startAt=" + startAt + "&maxResults=" + maxResults));
            int length = obj.getJSONArray("issues").length();
            for (int i = 0; i < length; i++) {
                URI original_uri = new URI(((JSONObject) obj.getJSONArray("issues").get(
                        i)).getString("self"));
                original_members.add(original_uri);
                // change URI for change event
                int id = ((JSONObject) obj.getJSONArray("issues").get(i)).getInt("id");
                URI uri = new URI(issues + id);
                members.add(uri);
                // write to changeLog
                if (run == false) { // first time run
                    String updateTime = ((JSONObject) obj.getJSONArray("issues").get(
                            i)).getJSONObject("fields").getString("updated");
                    lastUpdate = updateTime; // set update time
                    System.out.println("Change order: " + ++changeOrder + ". Type: create");
                    System.out.println("UPDATE: " + updateTime);
                    System.out.println("URI: " + uri);
                    ChangeEvent ce = new Creation(URI.create(
                            "urn:urn-3:cm1.example.com:" + updateTime + ":" + changeOrder), uri,
                            changeOrder);
                    changeLog.getChange().add(ce);
                }
            }
            if (length < maxResults) {
                break;
            }
            startAt = startAt + maxResults;
        }
        run = true;
        return members;
    }

}
