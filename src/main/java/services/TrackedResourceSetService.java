/*
 * Copyright (c) 2012, 2013, 2017 IBM Corporation and others.
 * Copyright 2017 Xufei Ning
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 * 
 * * Contributors:
 * 
 *    Susumu Fukuda - Initial implementation
 *    Xufei Ning    - Support for JIRA and WebHook
 */
 
 package services;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.eclipse.lyo.core.trs.AbstractChangeLog;
import org.eclipse.lyo.core.trs.ChangeLog;
import org.eclipse.lyo.core.trs.EmptyChangeLog;
import org.eclipse.lyo.core.trs.Page;
import org.eclipse.lyo.core.trs.TRSConstants;
import org.eclipse.lyo.core.trs.TrackedResourceSet;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.model.OslcMediaType;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trs.TRSHandler;

import org.eclipse.paho.client.mqttv3.MqttException;

@Path("trs")
public class TrackedResourceSetService {

	/**
	 * Server goes live when the TRS base is paged
	 */
	private static boolean LIVE_MODE = false; // server status
	private static Logger log = LoggerFactory.getLogger(TrackedResourceSetService.class);

	@Context
	private HttpServletRequest httpServletRequest;
	@Context
	private HttpServletResponse httpServletResponse;
	@Context
	private UriInfo uriInfo;

	static {
		log.info("Init TrackedResourceSetService");
		if(!LIVE_MODE) {
			log.info("LIVE mode OFF; generating TRS base");

			try {
				TRSHandler.generateBase();
				TRSHandler.baseTimer(); // regenerate base
				LIVE_MODE = true;
				log.info("LIVE mode ON");
			} catch (URISyntaxException | IOException | ParseException e) {
				log.error("Error generating the base in the static ctor");
			}
		}
	}

	@GET
	@Produces({ "text/plain", "text/html" })
	public String getTrackedResourceSetText() {
		return "Hello TRS service.";
	}

	@GET
	@Produces({ OslcMediaType.TEXT_TURTLE, OslcMediaType.APPLICATION_RDF_XML, OslcMediaType.APPLICATION_XML,
			OslcMediaType.APPLICATION_JSON })
	public TrackedResourceSet getTrackedResourceSet()
			throws URISyntaxException, JSONException, ClientProtocolException, IOException, ParseException {
		System.out.println("TRS request: " + httpServletRequest.getRequestURI());
		log.info("received request for trs at url:" + httpServletRequest.getRequestURI() + " . Processing request");

		TrackedResourceSet result = new TrackedResourceSet();
		result.setAbout(URI.create(TRSHandler.server_uri));//$NON-NLS-1$
		result.setBase(URI.create(TRSHandler.server_uri + "/" + TRSConstants.TRS_TERM_BASE));// $NON-NLS-1$

		AbstractChangeLog changeLog = (AbstractChangeLog) TRSHandler.changeLog;
		if (changeLog == null) {
			changeLog = new EmptyChangeLog();
		}
		result.setChangeLog(changeLog);

		return result;
	}

	@Path(TRSConstants.TRS_TERM_BASE)
	@GET
	@Produces({ OslcMediaType.TEXT_TURTLE, OslcMediaType.APPLICATION_RDF_XML, OslcMediaType.APPLICATION_XML,
			OslcMediaType.APPLICATION_JSON })
	public Page getBase() {
		URI requestURI = uriInfo.getRequestUri();
		boolean endsWithSlash = requestURI.getPath().endsWith("/");
		String redirectLocation = requestURI.toString() + (endsWithSlash ? "1" : "/1");
		try {
			throw new WebApplicationException(Response.temporaryRedirect(new URI(redirectLocation)).build());
		} catch (URISyntaxException e) {
			throw new IllegalStateException(e);
		}
	}

	@GET
	@Path(TRSConstants.TRS_TERM_BASE + "/{page}")
	@Produces({ OslcMediaType.TEXT_TURTLE, OslcMediaType.APPLICATION_RDF_XML, OslcMediaType.APPLICATION_XML,
			OslcMediaType.APPLICATION_JSON })
	public Page getBasePage(@PathParam("page") String pagenum)
			throws URISyntaxException, ClientProtocolException, IOException, JSONException, ParseException {
		log.info("received request for base page at url:" + httpServletRequest.getRequestURI()
				+ " . Processing request");
		System.out.println("Request from: " + httpServletRequest.getRequestURI());

		if (!LIVE_MODE) {
			log.error("Must have been done in the ctor");
		}
		Page page = TRSHandler.getPageResource(pagenum);
		return page;
	}

	@Path(TRSConstants.TRS_TERM_CHANGE_LOG)
	@GET
	@Produces({ OslcMediaType.TEXT_TURTLE, OslcMediaType.APPLICATION_RDF_XML, OslcMediaType.APPLICATION_XML,
			OslcMediaType.APPLICATION_JSON })
	public ChangeLog getChangeLog() {
		URI requestURI = uriInfo.getRequestUri();
		boolean endsWithSlash = requestURI.getPath().endsWith("/");
		String redirectLocation = requestURI.toString() + (endsWithSlash ? "1" : "/1");
		try {
			throw new WebApplicationException(Response.temporaryRedirect(new URI(redirectLocation)).build());
		} catch (URISyntaxException e) {
			throw new IllegalStateException(e);
		}
	}

	@GET
	@Path(TRSConstants.TRS_TERM_CHANGE_LOG + "/{page}")
	@Produces({ OslcMediaType.TEXT_TURTLE, OslcMediaType.APPLICATION_RDF_XML, OslcMediaType.APPLICATION_XML,
			OslcMediaType.APPLICATION_JSON })
	public ChangeLog getChangeLogPage(@PathParam("page") String pagenum) throws URISyntaxException {
		log.info("received request for changeLog page at url:" + httpServletRequest.getRequestURI()
				+ " . Processing request");
		System.out.println("Request from: " + httpServletRequest.getRequestURI());

		TRSHandler.generateChangeLog();
		ChangeLog changeLog = TRSHandler.getChangeLog(pagenum);
		return changeLog;
	}

	@POST
	@Path("issues/{type}")
	@Produces({ MediaType.TEXT_HTML })
	public void handleIssuesWebhooks(@PathParam("type") String type)
			throws ServletException, IOException, URISyntaxException, JSONException, ParseException, MqttException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException, OslcCoreApplicationException {
		log.info("New webhook message: type={}", type);
		// type is {create,modify,delete} - TRSHandler
		ServletInputStream inputStream = httpServletRequest.getInputStream();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonData = new HashMap<String, Object>();
		jsonData = mapper.readValue(inputStream, Map.class);
		LinkedHashMap<String, Object> issueData = (LinkedHashMap<String, Object>) jsonData.get("issue");
		JSONObject obj = new JSONObject(issueData);
		if (LIVE_MODE || true) {
			// TODO Andrew@2017-10-21: add logging 
			TRSHandler.addChangeEvent(type, obj); // add change event
		}
	}

}
